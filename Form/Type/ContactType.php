<?php


namespace App\Form\Type;


use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options): void
	{
		$builder
			->add(
				'firstName',
				TextType::class,
				[
					'constraints' => [
						new NotBlank(),
						new Length(['min' => 4]),
					],
				]
			)
			->add(
				'lastName',
				TextType::class,
				[
					'constraints' => [
						new NotBlank(),
						new Length(['min' => 4]),
					],
				]
			)
			->add(
				'email',
				EmailType::class,
				[
					'constraints' => [
						new NotBlank(),
						new Length(['min' => 4]),
					],
				]
			)
			->add(
				'phone',
				TelType::class,
				[
					'constraints' => [
						new Length(['min' => 10]),
					],
					'required' => false,
					'label' => 'Phone (optional)'

				]
			)
			->add(
				'message',
				TextareaType::class,
				[
					'constraints' => [
						new NotBlank(),
						new Length(['max' => 255]),
					],
					'attr' => ['rows' => '5']
				]
			)
			->add(
				'submit',
				SubmitType::class,
				[
					'label' => 'Submit',
				]
			)
			->setAction('/')
		;
	}

	public function configureOptions(OptionsResolver $resolver): void
	{
		$resolver->setDefaults(
			[
				'data_class' => Contact::class,
			]
		);
	}

}