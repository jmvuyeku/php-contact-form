<?php

use App\Database\DB;
use App\Database\DBException;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Dotenv\Dotenv;

define('VENDOR_DIR', realpath(__DIR__ . '/../vendor'));
define('FORM_DIR', VENDOR_DIR . '/symfony/form');
define('VALIDATOR_DIR', VENDOR_DIR . '/symfony/validator');
define('TWIG_BRIDGE_DIR', VENDOR_DIR . '/symfony/twig-bridge');
define('TEMPLATE_DIR', realpath(__DIR__ . '/../templates'));
define('PUBLIC_DIR', realpath(__DIR__ . '/../public'));
define('CACHE_DIR', realpath(__DIR__ . '/../cache'));


try {
	(new Dotenv())->loadEnv(__DIR__ . '/../.env');
	/**
	 * check if db exists
	 */
	if ($db = DB::connect()) {
		define('HAS_DB_SETUP', true);
		$sql = <<<SQL
CREATE TABLE IF NOT EXISTS `contact_message`(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `full_name` varchar(255) NOT NULL,
    `email` varchar(180) NOT NULL,
    `phone` varchar(80) NOT NULL,
    `message` varchar(255) NOT NULL,
    PRIMARY KEY (id))
    DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT ='Contact form table'
SQL;

		$db->exec($sql);
	}

} catch (Exception | DBException $e) {
	echo "<div class='alert alert-info text-center' id='notification-no-db'>{$e->getMessage()}</div>";
}

