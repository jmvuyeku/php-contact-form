#Getting MySQL docker instance started
Assuming you have docker installed on your machine
```
docker-compose up -d
```
Should you choose to have the db locally saved, add this line of code after the port config

```
//docker-compose.yml
    volumes:
      - ./docker/data:/var/lib/mysql
```