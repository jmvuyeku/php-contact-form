create table contact_message
(
    id        int(11) unsigned auto_increment
        primary key,
    full_name varchar(255) not null,
    email     varchar(180) not null,
    phone     varchar(80)  not null,
    message   varchar(255) not null
)
    comment 'Contact form table' collate = utf8_unicode_ci;

