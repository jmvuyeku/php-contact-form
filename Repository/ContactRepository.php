<?php
namespace App\Repository;

use App\Database\DB;
use App\Entity\Contact;

class ContactRepository
{
	/**
	 * @var DB
	 */
	private $db;
	/**
	 * @var Contact
	 */
	private $contact;

	/**
	 * ContactRepository constructor.
	 * @param Contact $contact
	 */
	public function __construct(Contact $contact)
	{
		$this->db = DB::connect();
		$this->contact = $contact;
	}

	/**
	 * @return bool
	 */
	public function save(): bool
	{
		$sql = 'INSERT INTO contact_message(full_name, email, phone, message) VALUES (:full_name, :email, :phone, :message)';

		$stmt = $this->db->prepare($sql);
		$stmt->bindValue(':full_name', $this->contact->getFullName(), DB::PARAM_STR);
		$stmt->bindValue(':email', $this->contact->getEmail(), DB::PARAM_STR);
		$stmt->bindValue(':phone', $this->contact->getPhone());
		$stmt->bindValue(':message', $this->contact->getMessage(), DB::PARAM_STR);

		if (!$stmt->execute()) {
			return false;
		}

		return true;

	}
}