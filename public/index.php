<?php

use App\Entity\Contact;
use App\Form\Type\ContactType;
use App\Page\ContactPage;
use Symfony\Component\Mime\Address;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../config/bootstrap.php';


try {
	// Create contact form!
	$contactPage = new ContactPage();
	$form = $contactPage->getFormBuilder()->create(ContactType::class);

	if (isset($_POST[$form->getName()])) {
		$form->submit($_POST[$form->getName()]);

		if ($form->isValid()) {
			$contactPage->saveCopy($form->getData());
			//change mailer dsn accordingly
			$contactPage->sendEmailWithContact(
				new Address('guy-smiley@example.com', 'Guy Smiley'),
				$form->getData(),
				'You have a new lead!!'
			);
			$message = "We received your submission, someone will be contacting you shortly. Thank you!";

			//clean up since it's a SPA site
			$form = $contactPage->getFormBuilder()->create(ContactType::class);
		}
	}

	echo $contactPage->getTwig()->render(
		'index.html.twig',
		[
			'form' => $form->createView(),
			'message' => $message ?? null,
		]
	);
} catch (Exception | Error $e) {
	echo $e->getMessage();
}