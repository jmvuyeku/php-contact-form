<?php

namespace App\Entity;

/**
 * Class Contact
 */
class Contact
{
	/**
	 * @var int
	 */
	private $id;
	/**
	 * @var string
	 */
	private $firstName;
	/**
	 * @var string
	 */
	private $lastName;
	/**
	 * @var string
	 */
	private $phone;
	/**
	 * @var string
	 */
	private $email;
	/**
	 * @var string
	 */
	private $message;

	/**
	 * @return string
	 */
	public function getMessage(): string
	{
		return $this->message;
	}

	/**
	 * @param string $message
	 * @return Contact
	 */
	public function setMessage(string $message): Contact
	{
		$this->message = $message;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 * @return Contact
	 */
	public function setId(int $id): Contact
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getFirstName(): string
	{
		return $this->firstName;
	}

	/**
	 * @param string $firstName
	 * @return Contact
	 */
	public function setFirstName(string $firstName): Contact
	{
		$this->firstName = $firstName;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getLastName(): string
	{
		return $this->lastName;
	}

	/**
	 * @param string $lastName
	 * @return Contact
	 */
	public function setLastName(string $lastName): Contact
	{
		$this->lastName = $lastName;
		return $this;
	}

	/**
	 * @return null|string
	 */
	public function getPhone(): ?string
	{
		return $this->phone;
	}

	/**
	 * @param string $phone
	 * @return Contact
	 */
	public function setPhone(string $phone): Contact
	{
		$this->phone = $phone;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getEmail(): string
	{
		return $this->email;
	}

	/**
	 * @param string $email
	 * @return Contact
	 */
	public function setEmail(string $email): Contact
	{
		$this->email = $email;
		return $this;
	}

	public function getFullName(): string
	{
		return "{$this->getFirstName()}, {$this->getLastName()}";
	}


}