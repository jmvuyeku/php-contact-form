<?php


namespace App\Traits;


use App\Entity\Contact;
use App\Repository\ContactRepository;

/**
 * Trait SaveMessage
 * @package App\Traits
 */
trait SaveMessage
{

	/**
	 * @param Contact $contact
	 * @return bool
	 */
	public function saveCopy(Contact $contact): bool
	{
		if(!defined('HAS_DB_SETUP')) {
			return false;
		}
		if (defined('HAS_DB_SETUP_TEST')) {
			return false;
		}

		return $this->getContactRepository($contact)->save();
	}

	/**
	 * @param Contact $contact
	 * @return ContactRepository
	 */
	private function getContactRepository(Contact $contact): ContactRepository
	{
		return new ContactRepository($contact);
	}
}