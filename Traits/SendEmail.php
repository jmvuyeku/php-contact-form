<?php


namespace App\Traits;


use App\Entity\Contact;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\Transport;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * Trait SendEmail
 * @package App\Traits
 */
trait SendEmail
{
	/**
	 * @var Environment
	 */
	private $twig;
	/**
	 * @var Mailer
	 */
	private $mailer;

	/**
	 * @param Address $addressTo
	 * @param Contact $contact
	 * @param string $subject
	 */
	public function sendEmailWithContact(
		Address $addressTo,
		Contact $contact,
		string $subject
	): void {
		try {
			$twig = new Environment(new FilesystemLoader(TEMPLATE_DIR));

			$transport = Transport::fromDsn($_ENV['MAILER_DSN']);
			$mailer = new Mailer($transport);


			$email = (new Email())
				->from('my@exampl.com')
				->to($addressTo)
				->subject($subject)
				->text($twig->render('email/contact.text.twig', ['contact' => $contact]))
				->html($twig->render('email/new.html.twig', ['contact' => $contact]));

			$mailer->send($email);
		} catch (TransportExceptionInterface $e) {
		}
	}


}
