<?php


namespace App\Traits;


use Symfony\Component\Security\Csrf\CsrfTokenManager;

/**
 * Trait TokenManager
 * @package App\Traits
 */
trait TokenManager
{
	/**
	 * @return CsrfTokenManager
	 */
	private function getCSRFTokenManager(): CsrfTokenManager
	{
		return new CsrfTokenManager();
	}

}