<?php
namespace App\Traits;

use Symfony\Component\Translation\Loader\XliffFileLoader;
use Symfony\Component\Translation\Translator;

/**
 * Trait HasTranslator
 * @package App\Traits
 */
trait HasTranslator
{
	/**
	 * @return Translator
	 */
	private function getTranslator(): Translator
	{
		$translator = new Translator('en');
		$translator->addLoader('xlf', new XliffFileLoader());
		$translator->addResource(
			'xlf',
			FORM_DIR . '/Resources/translations/validators.en.xlf',
			'en',
			'validators'
		);
		$translator->addResource(
			'xlf',
			VALIDATOR_DIR . '/Resources/translations/validators.en.xlf',
			'en',
			'validators'
		);

		return $translator;
	}
}