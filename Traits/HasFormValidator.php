<?php


namespace App\Traits;


use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Trait HasFormValidator
 * @package App\Traits
 */
trait HasFormValidator
{
	/**
	 * @return ValidatorInterface
	 */
	private function getValidator(): ValidatorInterface
	{
		return Validation::createValidator();
	}

}