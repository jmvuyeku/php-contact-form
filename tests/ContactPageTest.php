<?php


namespace App\tests;

require __DIR__ . '/../config/bootstrap.php';

use App\Entity\Contact;
use App\Form\Type\ContactType;
use App\Page\ContactPage;
use App\Page\PageInterface;
use PHPUnit\Framework\TestCase;

class ContactPageTest extends TestCase
{
	/**
	 * @var ContactPage
	 */
	private $contactPage;

	public function setUp(): void
	{
		$this->contactPage = new ContactPage();
		parent::setUp();
	}

	public function testShouldImplementPageInterface(): void
	{
		$this->assertInstanceOf(PageInterface::class, $this->contactPage);
	}

	public function testValidFormReturnsContactEntity(): Contact
	{
		$contact = new Contact();
		$contact->setFirstName('Jane')
			->setLastName('Smith')
			->setEmail('md@example.com')
			->setMessage('Hello There');
		$form = $this->contactPage->getFormBuilder()->create(ContactType::class, $contact);
		$this->assertInstanceOf(Contact::class, $form->getData());

		return $form->getData();
	}

	/**
	 * @depends testValidFormReturnsContactEntity
	 */
	public function testShouldNotSaveCopyIfNoDB(Contact $contact): void
	{
		define('HAS_DB_SETUP_TEST', true);

		$hasSaved = $this->contactPage->saveCopy($contact);
		$this->assertFalse($hasSaved);
	}
}