<?php

namespace App\Page;

use App\Traits\HasFormValidator;
use App\Traits\HasTranslator;
use App\Traits\SaveMessage;
use App\Traits\SendEmail;
use App\Traits\TokenManager;
use ReflectionClass;
use Symfony\Bridge\Twig\AppVariable;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Bridge\Twig\Form\TwigRendererEngine;
use Symfony\Component\Form\Extension\Csrf\CsrfExtension;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormRenderer;
use Symfony\Component\Form\Forms;
use Symfony\Component\Security\Csrf\CsrfTokenManager;
use Symfony\Component\Translation\Translator;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\RuntimeLoader\FactoryRuntimeLoader;

/**
 * Class ContactPage
 * @package App\Page
 */
class ContactPage implements PageInterface
{
	use HasTranslator;
	use TokenManager;
	use HasFormValidator;
	use SendEmail;
	use SaveMessage;

	/**
	 * @var Translator
	 */
	private $translator;
	/**
	 * @var CsrfTokenManager
	 */
	private $csrfTokenManager;

	/**
	 * ContactPage constructor.
	 */
	public function __construct()
	{
		$this->csrfTokenManager = $this->getCSRFTokenManager();
		$this->translator = $this->getTranslator();
	}

	/**
	 * @return Environment
	 */
	public function getTwig(): Environment
	{
		return $this->getTwigEnvironment();
	}

	/**
	 * @return Environment
	 */
	private function getTwigEnvironment(): Environment
	{
		$defaultFormTheme = 'bootstrap_3_layout.html.twig';
		$csrfTokenManager = $this->csrfTokenManager;

		$appVariableReflection = new ReflectionClass(AppVariable::class);
		$vendorTwigBridgeDir = dirname($appVariableReflection->getFileName());


		$twig = new Environment(
			new FilesystemLoader(
				[
					TEMPLATE_DIR,
					$vendorTwigBridgeDir . '/Resources/views/Form',
				]
			)
		);

		$formEngine = new TwigRendererEngine([$defaultFormTheme], $twig);
		$twig->addRuntimeLoader(
			new FactoryRuntimeLoader(
				[
					FormRenderer::class => static function () use ($formEngine, $csrfTokenManager) {
						return new FormRenderer($formEngine, $csrfTokenManager);
					},
				]
			)
		);

		$twig->addExtension(new TranslationExtension($this->translator));
		$twig->addExtension(new FormExtension());


		return $twig;
	}

	/**
	 * @return FormFactoryInterface
	 */
	public function getFormBuilder(): FormFactoryInterface
	{
		return $this->getFormComponent();
	}

	/**
	 * @return FormFactoryInterface
	 */
	private function getFormComponent(): FormFactoryInterface
	{
		return Forms::createFormFactoryBuilder()
			->addExtension(new CsrfExtension($this->csrfTokenManager))
			->addExtension(new ValidatorExtension($this->getValidator()))
			->getFormFactory();
	}

}