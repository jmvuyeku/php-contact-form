<?php


namespace App\Page;


use Symfony\Component\Form\FormFactoryInterface;
use Twig\Environment;

interface PageInterface
{
	/**
	 * @return Environment
	 */
	public function getTwig(): Environment;

	/**
	 * @return FormFactoryInterface
	 */
	public function getFormBuilder(): FormFactoryInterface;
}