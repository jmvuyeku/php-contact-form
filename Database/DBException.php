<?php


namespace App\Database;


use PDOException;
use Throwable;

class DBException extends PDOException
{
	public function __construct($message = "Database connection error occurred!", $code = 0, Throwable $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}