<?php

namespace App\Database;

use PDO;
use PDOException;

class DB extends PDO
{
	/**
	 * @var PDO
	 */
	private static $instance;

	/**
	 * @return PDO
	 */
	public static function connect(): PDO
	{
		if (self::$instance !== null) {
			return self::$instance;
		}
		try {
			$host = '127.0.0.1';
			$dbname = $_ENV['MYSQL_DATABASE'];
			$user = $_ENV['MYSQL_USER'];
			$pass = $_ENV['MYSQL_PASSWORD'];

			$dsn = "mysql:host={$host};dbname={$dbname};port=3310";
			self::$instance = new PDO($dsn, $user, $pass);
			return self::$instance;
		} catch (PDOException $e) {
			throw new DBException();
		}
	}

}